from sqlalchemy import Column, Integer, String, Text, Boolean
from sqlalchemy.sql.expression import false, true

from dao.datacon import Base


class Area(Base):
    __tablename__ = 'area'
    id = Column(Integer, primary_key=True)
    code = Column(String(length=200), nullable=false, unique=true)
    name = Column(String(length=100), nullable=false)
    parent = Column(String(length=10))
    default_area = Column(Boolean, default=false)

