from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.sql.sqltypes import Date

from dao.datacon import Base


class ReportType(Base):
    __tablename__ = 'report_type'
    id = Column(Integer, primary_key=True)
    category = Column(String(length=200))

