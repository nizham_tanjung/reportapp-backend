from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.sql.sqltypes import Date

from dao.datacon import Base


class ReportFile(Base):
    __tablename__ = 'report_files'
    id = Column(Integer, primary_key=True)
    report_id = Column(Integer)
    file_name = Column(Text)
    file_path = Column(Text)
