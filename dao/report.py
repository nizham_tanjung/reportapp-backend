from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.sql.sqltypes import Boolean, Date

from dao.datacon import Base


class Report(Base):
    __tablename__ = 'report'
    id = Column(Integer, primary_key=True)
    category = Column(String(length=200))
    what = Column(String(length=256))
    when = Column(Date)
    why = Column(Text)
    who = Column(Text)
    where = Column(Text)
    how = Column(Text)
    area = Column(String(length=50))
    classified = Column(Boolean)
    report_by = Column(String(length=100))
    report_date = Column(Date)
