import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

database_url = "mariadb+mariadbconnector://root:mysql@127.0.0.1:3306/reportapp_db"

engine = sqlalchemy.create_engine(database_url)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
