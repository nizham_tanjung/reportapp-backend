from pydantic import BaseModel
from datetime import date


class Account(BaseModel):
    id: int
    email: str
    username: str
    password: str
    role: str
    area: str


class Area(BaseModel):
    id: int
    code: str
    name: str
    parent: str
    default_area: bool


class Profile(BaseModel):
    id: int
    full_name: str
    photopath: str
    phone: str
    address: str
    full_name: str
    birth_place: str
    birth_date: str


class Report(BaseModel):
    id: int
    category: str
    what: str
    when: date
    why: str
    who: str
    where: str
    how: str
    area: str
    classified: bool
    report_by: str


class ReportType(BaseModel):
    id: int
    category: str


class ReportFiles(BaseModel):
    id: int
    report_id: int
    file_name: str
    file_path: str


class UserLogin(BaseModel):
    username: str
    password: str
