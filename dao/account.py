from sqlalchemy import Column, Integer, String, Text  # , Boolean

from dao.datacon import Base


class Account(Base):
    __tablename__ = 'account'
    id = Column(Integer, primary_key=True)
    email = Column(String(length=200), unique=True)
    username = Column(String(length=100), unique=True)
    password = Column(Text)
    role = Column(String(length=10))
    area = Column(String(length=10))
