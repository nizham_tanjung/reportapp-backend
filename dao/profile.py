from sqlalchemy import Column, Integer, String, Text  # , Boolean
from sqlalchemy.sql.sqltypes import Date

from dao.datacon import Base


class Profile(Base):
    __tablename__ = 'profile'
    id = Column(Integer, primary_key=True)
    full_name = Column(String(length=200))
    photopath = Column(Text)
    phone = Column(String(length=20))
    address = Column(Text)
    birth_place = Column(String(length=100))
    birth_date = Column(Date)
