from fastapi import APIRouter, Depends, HTTPException, status
from fastapi_jwt_auth import AuthJWT
from dao.datamod import Report
from service import report_srv
from sqlalchemy.orm import Session
from dao import datacon

report_route = APIRouter()


def get_db():
    db = datacon.SessionLocal()
    try:
        yield db
    finally:
        db.close()


@report_route.get("/report")
async def all_report(Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    reports = report_srv.select_all(db)
    return reports


@report_route.get("/report/by_area/{areaid}")
async def all_report(areaid: str, Authorize: AuthJWT = Depends(),
                     db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    reports = report_srv.select_all_by_area(db, areaid)
    return reports


@report_route.get("/report/{id}")
async def a_report(id: int, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    report = report_srv.select_one(db, id)
    return report


@report_route.get("/report/area/{area_code}/date/{sel_date}")
async def all_report(area_code: str, sel_date: str, Authorize: AuthJWT = Depends(),
                     db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    reports = report_srv.select_by_area(db, area_code, sel_date)
    return reports


@report_route.get("/report/sec_area/{area_code}/sec_date/{sel_date}")
async def secret_report(area_code: str, sel_date: str, Authorize: AuthJWT = Depends(),
                        db: Session = Depends(get_db)):
    # try:
    #     Authorize.jwt_required()
    # except Exception as e:
    #     raise HTTPException(
    #         status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    reports = report_srv.get_classified(db, area_code, sel_date)
    return reports


@report_route.post("/report")
async def add_report(report: Report, Authorize: AuthJWT = Depends(),
                     db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    reportid = report_srv.add_report(db, report.category, report.what, report.why, report.where, report.when,
                                     report.who, report.how, report.area, report.classified, report.report_by)
    return reportid


@report_route.put("/report")
async def update_report(report: Report, Authorize: AuthJWT = Depends(),
                        db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    report_srv.update_report(db, report.id, report.category, report.what, report.why,
                             report.where, report.when,
                             report.who, report.how, report.area, report.classified)
    return "Success update report"


@report_route.delete("/report/del/{reportid}")
async def delete_report(reportid: int, Authorize: AuthJWT = Depends(),
                        db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    report_srv.delete_report(db, reportid)
    return "Success delete report"
