from dao.datamod import Account
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi_jwt_auth import AuthJWT
from service import account_srv
from dao import datacon
from sqlalchemy.orm import Session


def get_db():
    db = datacon.SessionLocal()
    try:
        yield db
    finally:
        db.close()


account_route = APIRouter()


@account_route.get("/account")
async def all_account(db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    accounts = account_srv.select_all(db)
    return accounts


@account_route.get("/account/{id}")
async def an_account(id: int, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    account = account_srv.select_one(db, id)
    return account


@account_route.get("/account/name/{username}")
async def account_by_name(username: str, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    account = account_srv.select_by_username(db, username)
    return account


@account_route.post("/account")
async def add_account(account: Account, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    account_srv.add_account(db, account.email, account.username,
                            account.password, account.role, account.area)
    return "Success add new account"


@account_route.put("/account")
async def update_account(account: Account, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    account_srv.update_account(db,
                               account.id, account.email, account.username, account.password, account.role, account.area)
    return "Success update account"


@account_route.delete("/account/del/{accountid}")
async def delete_account(accountid: int, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    account_srv.delete_account(db, accountid)
    return "Success delete account"
