from fastapi import APIRouter, Depends, HTTPException, status
from fastapi_jwt_auth import AuthJWT
from dao.datamod import Profile
from service import profile_srv
from sqlalchemy.orm import Session
from dao import datacon

profile_route = APIRouter()


def get_db():
    db = datacon.SessionLocal()
    try:
        yield db
    finally:
        db.close()


@profile_route.get("/profile")
async def all_area(Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    profiles = profile_srv.select_all(db)
    return profiles


@profile_route.get("/profile/{id}")
async def an_area(id: int, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    profile = profile_srv.select_one(db, id)
    return profile


@profile_route.post("/profile")
async def add_area(profile: Profile, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    profile_srv.add_profile(db, profile.full_name, profile.photopath,
                            profile.phone, profile.address, profile.birth_place, profile.birth_date)
    return "Success add new profile"


@profile_route.put("/profile")
async def update_area(profile: Profile, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    profile_srv.update_profile(db, profile.id, profile.full_name, profile.photopath,
                               profile.phone, profile.address, profile.birth_place, profile.birth_date)
    return "Success update profile"


@profile_route.delete("/profile/del/{profileid}")
async def delete_profile(profileid: int, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    profile_srv.delete_profile(db, profileid)
    return "Success delete profile"
