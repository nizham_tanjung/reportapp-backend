from fastapi import APIRouter, Depends, HTTPException, status
from fastapi_jwt_auth import AuthJWT
from dao.datamod import ReportType
from service import report_type_srv as rt_srv

from sqlalchemy.orm import Session
from dao import datacon

rtype_route = APIRouter()


def get_db():
    db = datacon.SessionLocal()
    try:
        yield db
    finally:
        db.close()


@rtype_route.get("/report_type")
async def all_report_type(Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    report_types = rt_srv.select_all(db)
    return report_types


@rtype_route.get("/report_type/{id}")
async def an_report_type(id: int, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    report_type = rt_srv.select_one(db, id)
    return report_type


@rtype_route.post("/report_type")
async def add_report_type(report_type: ReportType, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    rt_srv.add_type(db, report_type.category)
    return "Success add new report_type"


@rtype_route.put("/report_type")
async def update_report_type(report_type: ReportType, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    rt_srv.update_type(db, report_type.id, report_type.category)
    return "Success update report_type"


@rtype_route.delete("/report_type/del/{typeid}")
async def delete_report_type(typeid: int, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    rt_srv.delete_type(db, typeid)
    return "Success delete report_type"
