from dao.datamod import Area
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi_jwt_auth import AuthJWT
from service import area_srv
from sqlalchemy.orm import Session
from dao import datacon

area_route = APIRouter()


def get_db():
    db = datacon.SessionLocal()
    try:
        yield db
    finally:
        db.close()


@area_route.get("/area")
async def all_area(db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    areas = area_srv.select_all(db)
    return areas


@area_route.get("/area/{id}")
async def an_area(id: int, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    area = area_srv.select_one(db, id)
    return area


@area_route.get("/area/c/{code}")
async def area_by_code(code: str, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    area = area_srv.select_by_code(db, code)
    return area


@area_route.post("/area")
async def add_area(area: Area, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    area_srv.add_area(db, area.code, area.name, area.parent, area.default_area)
    return "Success add new area"


@area_route.put("/area", status_code=status.HTTP_202_ACCEPTED)
async def update_area(area: Area, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    area_srv.update_area(db, area.id, area.code, area.name, area.parent, False)
    return "Success update area"


@area_route.delete("/area/del/{areaid}")
async def delete_area(areaid: int, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    area_srv.delete_area(db, areaid)
    return "Success delete area"
