from os import path
from typing import List

from fastapi import APIRouter, Depends, HTTPException, status, File, UploadFile
from fastapi_jwt_auth import AuthJWT
from dao.datamod import ReportFiles
from service import report_files_srv as rt_srv

from PIL import Image

from sqlalchemy.orm import Session
from dao import datacon

repfile_route = APIRouter()

base_path = 'dataimg'


def get_db():
    db = datacon.SessionLocal()
    try:
        yield db
    finally:
        db.close()


@repfile_route.get("/report_files/{id}")
async def an_report_file(id: int, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    report_file = rt_srv.select_one(db, id)
    return report_file


@repfile_route.get("/report_files/report/{rid}")
async def a_report_file(rid: int, db: Session = Depends(get_db)):
    # , Authorize: AuthJWT = Depends()
    # try:
    #     Authorize.jwt_required()
    # except Exception as e:
    #     raise HTTPException(
    #         status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    report_files = rt_srv.select_by_report(db, rid=rid)
    return report_files


@repfile_route.post("/report_files/register")
async def register_files(report_file: ReportFiles, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    rt_srv.addreport_file(db,
                          report_file.report_id, report_file.file_name, report_file.file_path)
    return "Success update report_file"

# @repfile_route.post("/report_files")
# async def add_report_file(report_file: ReportFiles, Authorize: AuthJWT = Depends()):
#     try:
#         Authorize.jwt_required()
#     except Exception as e:
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
#     rt_srv.add_type(report_file.category)
#     return "Success add new report_file"


# @repfile_route.post("/report_files/uploadfiles/")
# async def create_upload_files(files: List[UploadFile] = File(...)):
#     for file in files:
#         print(file.filename)
#     return {"filenames": [file.filename for file in files]}


@repfile_route.post("/report_files/uploadfile/")
async def create_upload_files(file: UploadFile = File(...)):
    with Image.open(file.file) as im:
        if (path.isdir('dataimg')):
            im.save(path.join(base_path, file.filename))
        else:
            print("File cannot be saved")
    print(file.filename)
    return {"filename": file.filename}


@repfile_route.put("/report_files")
async def update_report_file(report_file: ReportFiles, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    rt_srv.updatereport_file(db, report_file.id, report_file.category)
    return "Success update report_file"


@repfile_route.delete("/report_files/del/{typeid}")
async def delete_report_file(typeid: int, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        Authorize.jwt_required()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Unauthorized action!')
    rt_srv.deletereport_file(db, typeid)
    return "Success delete report_file"
