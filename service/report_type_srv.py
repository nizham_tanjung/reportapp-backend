from dao.report_type import ReportType
from sqlalchemy.orm import Session


def add_type(db: Session, category):
    new_type = ReportType()
    new_type.category = category

    db.add(new_type)
    db.commit()
    db.refresh()
    return new_type


def select_all(db: Session):
    types = db.query(ReportType).all()
    return types


def select_one(db: Session, id):
    areas = db.query(ReportType).get(id)
    return areas


def update_type(db: Session, id, category):
    type = db.query(ReportType).get(id)
    type.category = category
    db.commit()


def delete_type(db: Session, id):
    db.query(ReportType).filter(ReportType.id == id).delete()
    db.commit()
