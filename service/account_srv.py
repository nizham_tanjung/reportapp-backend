from sqlalchemy.orm import Session
from dao.account import Account
from sqlalchemy import select


def add_account(db: Session, email, username, password, role, area):
    new_account = Account()
    new_account.email = email
    new_account.username = username
    new_account.password = password
    new_account.role = role
    new_account.area = area

    db.add(new_account)
    db.commit()
    db.refresh(new_account)
    return new_account


def select_all(db: Session):
    accounts = db.query(Account).all()
    return accounts


def select_by_username(db: Session, username):
    sql = select(Account).where(Account.username == username)
    result = db.execute(sql)
    for acc in result.scalars():
        print(f"{acc.username} {acc.role}")
    return acc


def select_one(db: Session, id: int):
    account = db.query(Account).get(id)
    return account


def update_account(db: Session, id, email, username, password, role, area):
    account = db.query(Account).get(id)
    account.email = email
    account.username = username
    account.password = password
    account.role = role
    account.area = area
    db.commit()
    db.refresh(account)
    return account


def change_password(db: Session, id, username, old_pwd, new_pwd):
    account = db.query(Account).get(id)
    if (account.username == username and account.password == old_pwd):
        account.password == new_pwd
        db.commit()
        return True
    return False


def isAuthentic(db: Session, username, password):
    rows = db.query(Account).filter_by(
        username=username, password=password).count()
    if(rows > 0):
        return True
    return False


def delete_account(db: Session, id):
    db.query(Account).filter(Account.id == id).delete()
    db.commit()
