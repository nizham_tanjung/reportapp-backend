from sqlalchemy.orm import Session
from dao.area import Area


def add_area(db: Session, code, name, parent, default_area):
    new_area = Area()
    new_area.code = code
    new_area.name = name
    new_area.parent = parent
    new_area.default_area = default_area
    db.add(new_area)
    db.commit()
    db.refresh(new_area)
    return new_area


def select_all(db: Session):
    areas = db.query(Area).all()
    return areas


def select_one(db: Session, id):
    area = db.query(Area).get(id)
    return area


def select_by_code(db: Session, area_code: str):
    selected_area = db.query(Area).filter_by(code=area_code).one_or_none()
    return selected_area


def update_area(db: Session, id, code, name, parent, default_area):
    area = db.query(Area).get(id)
    area.code = code
    area.name = name
    area.parent = parent
    area.default_area = default_area
    db.commit()


def delete_area(db: Session, id):
    db.query(Area).filter(Area.id == id).delete()
    db.commit()
