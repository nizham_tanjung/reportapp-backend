from dao.report_files import ReportFile
from sqlalchemy.orm import Session


def addreport_file(db: Session, report_id, filename, filepath):
    newreport_file = ReportFile()
    newreport_file.report_id = report_id
    newreport_file.file_name = filename
    newreport_file.file_path = filepath
    db.add(newreport_file)
    db.commit()
    db.refresh(newreport_file)
    return newreport_file


def select_all(db: Session):
    rp_files = db.query(ReportFile).all()
    return rp_files


def select_one(db: Session, id):
    rp_file = db.query(ReportFile).get(id)
    return rp_file


def select_by_report(db: Session, rid):
    rp_files = db.query(ReportFile).filter_by(report_id=rid).all()
    return rp_files


def updatereport_file(db: Session, id, report_id, filename, filepath):
    report_file = db.query(ReportFile).get(id)
    report_file.full_name = report_id
    report_file.photopath = filename
    report_file.phone = filepath
    db.commit()
    db.refresh(report_file)
    return report_file


def deletereport_file(db: Session, id):
    db.query(ReportFile).filter(ReportFile.id == id).delete()
    db.commit()
