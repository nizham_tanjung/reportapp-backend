from sqlalchemy.sql.expression import extract
from dao.report import Report
from datetime import date
from sqlalchemy.orm import Session


def add_report(db: Session, category, what, why, where, when,
               who, how, area, classified, report_by):
    new_report = Report()
    new_report.category = category
    new_report.what = what
    new_report.why = why
    new_report.where = where
    new_report.when = when
    new_report.who = who
    new_report.how = how
    new_report.area = area
    new_report.classified = classified
    new_report.report_by = report_by
    new_report.report_date = date.today()
    db.add(new_report)
    db.commit()
    db.refresh(new_report)
    return new_report.id


def select_all(db: Session):
    reports = db.query(Report).filter(Report.classified == False).order_by(
        Report.when.desc()).limit(200).all()
    return reports


def select_all_by_area(db: Session, sel_area):
    reports = db.query(Report).filter(Report.area == sel_area, Report.classified == False).order_by(
        Report.when.desc()).limit(200).all()
    return reports


def select_by_area(db: Session, sel_area, sel_date):
    reports = db.query(Report).filter(Report.area == sel_area, extract(
        'month', Report.when) == sel_date, Report.classified == False).all()
    return reports


def get_classified(db: Session, sel_area, sel_date):
    reports = db.query(Report).filter(
        extract('month', Report.when) == sel_date, Report.classified == True,
        Report.area == sel_area).all()
    return reports


def select_one(db: Session, id):
    report = db.query(Report).get(id)
    return report


def update_report(db: Session, id, category, what, why, where,
                  when, who, how, area, classified):
    report = db.query(Report).get(id)
    report.category = category
    report.what = what
    report.why = why
    report.where = where
    report.when = when
    report.who = who
    report.how = how
    report.area = area
    report.classified = classified
    db.commit()
    db.refresh(report)
    return report


def delete_report(db: Session, id):
    db.query(Report).filter(Report.id == id).delete()
    db.commit()
