from sqlalchemy.orm import Session
from dao.profile import Profile


def add_profile(db: Session, full_name, photopath, phone, address, birth_place, birth_date):
    new_profile = Profile()
    new_profile.full_name = full_name
    new_profile.photopath = photopath
    new_profile.phone = phone
    new_profile.address = address
    new_profile.birth_place = birth_place
    new_profile.birth_date = birth_date
    db.add(new_profile)
    db.commit()
    db.refresh(new_profile)
    return new_profile


def select_all(db: Session):
    profiles = db.query(Profile).all()
    return profiles


def select_one(db: Session, id):
    profile = db.query(Profile).get(id)
    return profile


def update_profile(db: Session, id, full_name, photopath, phone,
                   address, birth_place, birth_date):
    profile = db.query(Profile).get(id)
    profile.full_name = full_name
    profile.photopath = photopath
    profile.phone = phone
    profile.address = address
    profile.birth_place = birth_place
    profile.birth_date = birth_date
    db.commit()
    db.refresh(profile)
    return profile


def delete_profile(db: Session, id):
    db.query(Profile).filter(Profile.id == id).delete()
    db.commit()
