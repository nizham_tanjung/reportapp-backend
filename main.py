from pydantic.main import BaseModel
from resource.account_api import account_route
from resource.area_api import area_route
from resource.profile_api import profile_route
from resource.report_api import report_route
from resource.report_files_api import repfile_route

import uvicorn
from fastapi import FastAPI, Depends, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware

from fastapi_jwt_auth import AuthJWT

from dao import datacon, datamod
from service import account_srv

from fastapi.staticfiles import StaticFiles

from sqlalchemy.orm import Session

app = FastAPI()


origins = ['*', 'http://localhost:4200']

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/static", StaticFiles(directory="static"), name="static")
app.mount("/dataimg", StaticFiles(directory="dataimg"), name="dataimg")


#-------Security----------#
class Settings(BaseModel):
    authjwt_secret_key = 'b0c67e4a3a7d0586065ab39f1136759fb449beaffd88f7e29a44a25b66bac273'


@AuthJWT.load_config
def get_config():
    return Settings()


def get_db():
    db = datacon.SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post('/login')
def login(user: datamod.UserLogin, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    if account_srv.isAuthentic(db, user.username, user.password):
        access_token = Authorize.create_access_token(subject=user.username)
        # refresh_token = Authorize.create_refresh_token(subject=user.username)
        return {'access_token': access_token}
        # , 'refresh_token': refresh_token

    raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                        detail='Invalid credentials!')
#-------End of Security----------#


@app.get("/")
async def root():
    return {"message": "ReportApp API"}

app.include_router(account_route)
app.include_router(area_route)
app.include_router(profile_route)
app.include_router(report_route)
app.include_router(repfile_route)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0")
